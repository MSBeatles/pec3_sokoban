using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VictoryScreen : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayAnother()
    {
        SceneManager.LoadScene("LevelList");
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("ActualMainMenu");
    }
}
