using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public Slider height;
    public Slider width;
    public Text currentHeight;
    public Text currentWidth;

    public Button createLevelButton;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        currentHeight.text = " " + height.value;
        currentWidth.text = " " + width.value;
        //Debug.Log("PlayerPrefs.Height: " + PlayerPrefs.GetFloat("Height"));
        //Debug.Log("PlayerPrefs.Width: " + PlayerPrefs.GetFloat("Width"));
    }

    public void CreateLevel()
    {
        PlayerPrefs.SetFloat("Height", height.value);
        PlayerPrefs.SetFloat("Width", width.value);
        SceneManager.LoadScene("LevelEditor");
    }


}
