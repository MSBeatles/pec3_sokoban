using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : MonoBehaviour
{
    private enum Tile { Floor, Box, Wall, Goal, Player };
    private Tile[,] tiles;
    private GameObject[,] myObjects;
    private Vector3 playerPos;
    private int boxes;
    private int goals;

    private bool moving;
    private Vector3 currentPos;
    private Vector3 goalPos;
    private int maxY;
    private int maxX;
    private int currentX;
    private int currentY;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void OnTriggerEnter2D(Collider2D coll)
    {

    }
}
