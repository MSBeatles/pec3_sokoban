/**using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    private enum Tile { Floor, Box, Wall, Goal, Player };
    private Tile[,] tiles;
    private GameObject[,] myObjects;
    private Vector3 playerPos;
    private int boxes;
    private int goals;

    private bool moving;
    private Vector3 currentPos;
    private Vector3 goalPos;
    private int maxY;
    private int maxX;
    private int currentX;
    private int currentY;

    // Start is called before the first frame update
    void Start()
    {
        tiles = new Tile[(int)PlayerPrefs.GetFloat("Width"), (int)PlayerPrefs.GetFloat("Height")];
        myObjects = new GameObject[(int)PlayerPrefs.GetFloat("Width"), (int)PlayerPrefs.GetFloat("Height")];
        Vector3 playerPos = new Vector3(0.0f, 0.0f, 0.0f);
        boxes = PlayerPrefs.GetInt("LevelBoxes");
        moving = false;
        currentPos = transform.position;
        maxY = (int)PlayerPrefs.GetFloat("Height");
        maxX = (int)PlayerPrefs.GetFloat("Width");
        currentPos = transform.position;


        string path = Application.persistentDataPath + "\\" + PlayerPrefs.GetString("ChosenLevel");
        //Debug.Log(path);
        StreamReader reader = new StreamReader(path, true);

        string line;
        line = reader.ReadLine();
        int.TryParse(line, out maxX);
        PlayerPrefs.SetFloat("Width", maxX);
        line = reader.ReadLine();
        int.TryParse(line, out maxY);
        PlayerPrefs.SetFloat("Height", maxY);

        for (int j = 0; j < maxY; j++)
        {
            line = "";
            line = reader.ReadLine();
            Debug.Log(line);
            for (int i = 0; i < maxX; i++)
            {
                if (line[i] == 'W')
                {
                    tiles[i, j] = Tile.Wall;
                }
                else if (line[i] == 'G')
                {
                    tiles[i, j] == Tile.Goal;
                }
                else if (line[i] == 'F')
                {
                    tiles[i, j] = Tile.Floor;
                }
                else if (line[i] == 'B')
                {
                    tiles[i, j] = Tile.Box;
                }
                else if (line[i] == 'P')
                {
                    tiles[i, j] = Tile.Player;
                }
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (!moving)
        {
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                if (CanGoDown())
                {
                    MoveDown();
                }
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                if (CanGoUp())
                {
                    MoveUp();
                }
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                if (CanGoLeft())
                {
                    MoveLeft();
                }
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                if (CanGoRight())
                {
                    MoveRight();
                }
            }
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, goalPos, 1.5f * Time.deltaTime);
            if (Mathf.Abs(goalPos.y - transform.position.y) < 0.0001f && Mathf.Abs(goalPos.x - transform.position.x) < 0.0001)
            {
                moving = false;
                currentPos = goalPos;
            }
        }
    }


    public void MoveDown()
    {
        moving = true;
        goalPos = currentPos;
        goalPos.y -= 1.0f;
    }

    public void MoveUp()
    {
        moving = true;
        goalPos = currentPos;
        goalPos.y += 1.0f;
    }

    public void MoveLeft()
    {
        moving = true;
        goalPos = currentPos;
        goalPos.x -= 1.0f;
    }

    public void MoveRight()
    {
        moving = true;
        goalPos = currentPos;
        goalPos.x += 1.0f;
    }

    public bool CanGoDown()
    {
        bool canGo = true;
        if(currentPos.y == 0)
        {
            canGo = false;
        }
        else if ()
    }

    public bool CanGoUp()
    {
        return currentPos.y < maxY - 1;
    }

    public bool CanGoLeft()
    {
        return currentPos.x > 0;
    }

    public bool CanGoRight()
    {
        return currentPos.x < maxX - 1;
    }

}
**/