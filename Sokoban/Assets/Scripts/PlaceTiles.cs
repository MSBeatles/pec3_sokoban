using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PlaceTiles : MonoBehaviour
{

    private enum Tile { Floor, Box, Wall, Goal, Player };
    private Tile[,] tiles;
    private GameObject[,] myObjects;
    private Vector3 playerPos;
    private int boxes;
    private int goals;

    private bool moving;
    private Vector3 currentPos;
    private Vector3 goalPos;
    private int maxY;
    private int maxX;
    private int currentX;
    private int currentY;

    public GameObject floor;
    public GameObject box;
    public GameObject wall;
    public GameObject goal;
    public GameObject player;

    public Transform camera;

    public Text msgText;


    // Start is called before the first frame update
    void Start()
    {
        tiles = new Tile[(int)PlayerPrefs.GetFloat("Width"), (int)PlayerPrefs.GetFloat("Height")];
        myObjects = new GameObject[(int)PlayerPrefs.GetFloat("Width"), (int)PlayerPrefs.GetFloat("Height")];
        Vector3 playerPos = new Vector3(0.0f, 0.0f, 0.0f);
        boxes = 0;
        goals = 0;
        moving = false;
        currentPos = transform.position;
        maxY = (int) PlayerPrefs.GetFloat("Height");
        maxX = (int) PlayerPrefs.GetFloat("Width");
        msgText.gameObject.SetActive(false);


        if (PlayerPrefs.HasKey("Levels"))
        {
            PlayerPrefs.SetInt("Levels", PlayerPrefs.GetInt("Levels") + 1);
        }
        else
        {
            PlayerPrefs.SetInt("Levels", 1);
        }
        Debug.Log("" + maxX);
        Debug.Log("" + maxY);

        for (int i = -1; i <= maxX; i++)
        {
            for (int j = -1; j <= maxY; j++)
            {
                if (i == -1 || j == -1 || i == maxX || j == maxY)
                {
                    Instantiate(wall, new Vector3(i, j, 0.0f), Quaternion.identity);
                }
                else if (i == 0 && j == 0)
                {
                    myObjects[i, j] = Instantiate(player, new Vector3(i, j, 0.0f), Quaternion.identity);
                    Instantiate(floor, new Vector3(i, j, 0.0f), Quaternion.identity);
                    tiles[i, j] = Tile.Player;
                    playerPos = new Vector3(0.0f, 0.0f, 0.0f);
                }
                else
                {
                    Instantiate(floor, new Vector3(i, j, 0.0f), Quaternion.identity);
                    tiles[i, j] = Tile.Floor;

                }
            }
        }

        camera.Translate(new Vector3((maxX / 2.0f) - 0.5f, (maxY / 2.0f) - 0.5f, 0.0f));
    }

    // Update is called once per frame
    void Update()
    {
        if (!moving)
        {
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                if (CanGoDown())
                {
                    MoveDown();
                }
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                if (CanGoUp())
                {
                    MoveUp();
                }
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                if (CanGoLeft())
                {
                    MoveLeft();
                }
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                if (CanGoRight())
                {
                    MoveRight();
                }
            }
            else if (Input.GetKeyDown(KeyCode.Space))
            {
                if (tiles[currentX, currentY] != Tile.Player)
                {
                    SwapTile();
                }
            }
            else if (Input.GetKeyDown(KeyCode.P))
            {
                SetPlayer();
            }
            else if (Input.GetKeyDown(KeyCode.Return))
            {
                SaveLevel();
            }
            else if (Input.GetKeyDown(KeyCode.Escape))
            {
                SaveLevel();
                SceneManager.LoadScene("ActualMainMenu");
            }
        }
        else
        {
            transform.Translate(goalPos - currentPos);
            if (Mathf.Abs(goalPos.y - transform.position.y) < 0.0001f && Mathf.Abs(goalPos.x - transform.position.x) < 0.0001)
            {
                moving = false;
                currentPos = goalPos;
                currentX = (int)currentPos.x;
                currentY = (int)currentPos.y;
            }
        }
    }

    /**************************************************************
     ******************MOVEMENT FUNCTIONS**************************
     **************************************************************/
    public void MoveDown()
    {
        moving = true;
        goalPos = currentPos;
        goalPos.y -= 1.0f;
    }

    public void MoveUp()
    {
        moving = true;
        goalPos = currentPos;
        goalPos.y += 1.0f;
    }

    public void MoveLeft()
    {
        moving = true;
        goalPos = currentPos;
        goalPos.x -= 1.0f;
    }

    public void MoveRight()
    {
        moving = true;
        goalPos = currentPos;
        goalPos.x += 1.0f;
    }

    public bool CanGoDown()
    {
        return currentPos.y > 0;
    }

    public bool CanGoUp()
    {
        return currentPos.y < maxY - 1;
    }

    public bool CanGoLeft()
    {
        return currentPos.x > 0;
    }

    public bool CanGoRight()
    {
        return currentPos.x < maxX - 1;
    }
    

    //Swaps the tile currently highlighted
    public void SwapTile()
    {
        //Primer destruim l'objecte que hi havia anteriorment, si n'hi havia un
        if (myObjects[currentX, currentY] != null)
        {
            Destroy(myObjects[currentX, currentY]);
        }

        //Despr�s passem al seg�ent objecte de l'Enum (excloent a Player, que �s un cas especial).
        int length = System.Enum.GetNames(typeof(Tile)).Length - 1;
        Tile currentTile = tiles[currentX, currentY];
        //Debug.Log(currentTile);
        tiles[currentX, currentY] = (Tile)(((int)currentTile + 1) % length);
        
        //Actualitzem la currentTile
        currentTile = tiles[currentX, currentY];
        
        //Instanciem l'objecte que toqui i el guardem a la llista d'objectes.
        if (currentTile == Tile.Box)
        {
            myObjects[currentX, currentY] = Instantiate(box, currentPos, Quaternion.identity);
            //Debug.Log("BOX");
        }
        else if (currentTile == Tile.Wall)
        {
            myObjects[currentX, currentY] = Instantiate(wall, currentPos, Quaternion.identity);
            //Debug.Log("WALL");
        }
        else if (currentTile == Tile.Goal)
        {
            myObjects[currentX, currentY] = Instantiate(goal, currentPos, Quaternion.identity);
            //Debug.Log("GOAL");
        }
        //Finalment guardarem a la llista d'objectes a X Y que hi ha l'objecte tal
        
    }

    public void SetPlayer()
    {
        //Esborrem l'objecte que hi hagu�s en aquesta casella
        if (myObjects[currentX, currentY] != null)
        {
            Destroy(myObjects[currentX, currentY]);
        }
        //Esborrem l'altre jugador i creem el nou
        if (tiles[(int)playerPos.x, (int)playerPos.y] == Tile.Player)
        {
            Destroy(myObjects[(int)playerPos.x, (int)playerPos.y]);
        }
        myObjects[currentX, currentY] = Instantiate(player, currentPos, Quaternion.identity);

        //Guardem el nou player a la llista de tiles, i actualitzem playerPos.
        tiles[currentX, currentY] = Tile.Player;
        tiles[(int)playerPos.x, (int)playerPos.y] = Tile.Floor;
        playerPos = currentPos;



    }

    public void SaveLevel()
    {
        
        if (CheckElements())
        {
            string path = Application.persistentDataPath + "/Level" + PlayerPrefs.GetInt("Levels");
            StreamWriter writer = new StreamWriter(path, false);

            string line = maxX.ToString();
            writer.WriteLine(line);
            line = maxY.ToString();
            writer.WriteLine(line);
            for (int j = 0; j < maxY; j++)
            {
                line = "";
                for (int i = 0; i < maxX; i++)
                {
                    string tile_letter = tiles[i, j] + " ";
                    line = line + tile_letter[0];
                }
                //Debug.Log(line);
                writer.WriteLine(line);
            }

            writer.Close();
        }
        else
        {
            msgText.gameObject.SetActive(true);
            StartCoroutine(FadeTextToZeroAlpha(1.5f, msgText));
        }
        
    }


    public void NextType()
    {
        int length = System.Enum.GetNames(typeof(Tile)).Length - 1;
        Tile currentTile = tiles[currentX, currentY];
        tiles[currentX, currentY] = (Tile)(((int)currentTile + 1) % length);
    }

    public bool CheckElements()
    {
        goals = 0;
        boxes = 0;
        for (int i = 0; i < maxX; i++)
        {
            for (int j = 0; j < maxY; j++)
            {
                if (tiles[i, j] == Tile.Goal)
                {
                    goals++;
                }
                else if (tiles[i, j] == Tile.Box)
                {
                    boxes++;
                }
            }
        }
        return (boxes == goals);
    }


    public IEnumerator FadeTextToZeroAlpha(float t, Text i)
    {
        i.color = new Color(i.color.r, i.color.g, i.color.b, 1);
        while (i.color.a > 0.0f)
        {
            i.color = new Color(i.color.r, i.color.g, i.color.b, i.color.a - (Time.deltaTime / t));
            yield return null;
        }
    }
}
