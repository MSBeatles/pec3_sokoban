using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.SceneManagement;

public class LoadLevelList : MonoBehaviour
{
    public Transform AnswersParent;
    public GameObject ButtonAnswerPrefab;

    // Start is called before the first frame update
    void Start()
    {
        FillUI();    
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FillUI()
    {
        //We will always write history into HistoryText.

        var isLeft = true;
        var height = 700.0f;
        var index = 0;
        string path = Application.persistentDataPath;
        foreach (string file in System.IO.Directory.GetFiles(path))
        {
            GameObject buttonAnswerCopy = Instantiate(ButtonAnswerPrefab, AnswersParent, true);

            var x = buttonAnswerCopy.GetComponent<RectTransform>().rect.x * 1.5f;
            buttonAnswerCopy.GetComponent<RectTransform>().localPosition = new Vector3(isLeft ? x : -x, height, 0);

            if (!isLeft)
                height += buttonAnswerCopy.GetComponent<RectTransform>().rect.y * 3.0f;
            isLeft = !isLeft;


            string levelName = "";
            string levelAssigned = "";
            bool found = false;
            for (int letter = 0; letter < file.Length; letter++)
            {
                if (!found)
                {
                    if (file.Substring(letter, 5) == "Level")
                    {
                        levelName = "Level ";
                        levelName += file.Substring(letter + 5, file.Length - letter - 5);
                        levelAssigned = file.Substring(letter, file.Length - letter);
                        found = true;
                    }
                }
            }
            FillListener(buttonAnswerCopy.GetComponent<Button>(), levelAssigned);
            buttonAnswerCopy.GetComponentInChildren<Text>().text = levelName;

            index++;
        }
    }

    private void FillListener(Button button, string file)
    {
        button.onClick.AddListener(() => { AnswerSelected(file); });
    }

    private void AnswerSelected(string file)
    {
        PlayerPrefs.SetString("ChosenLevel", file);
        //Debug.Log(PlayerPrefs.GetString("ChosenLevel"));
        SceneManager.LoadScene("LevelPlayer");
    }

}
